//
//  ContestView.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/8/24.
//

import SwiftUI

struct ContestView: View {
    @StateObject var viewModel = ContestsViewModel()
    var body: some View {
        List {
            ForEach(viewModel.contests) { contest in

                HStack(spacing: 12) {
                    Text("\(contest.title)")
                        .foregroundColor(.gray)
                    Text("\(contest.type)")
                        .foregroundColor(.gray)
                    VStack(alignment: .leading, spacing: 4) {
                        Text(viewModel.dateFormatter.string(from: NSDate(timeIntervalSince1970: TimeInterval(contest.startTs)) as Date))
                            .fontWeight(.semibold)
                        Text(viewModel.dateFormatter.string(from: NSDate(timeIntervalSince1970: TimeInterval(contest.endTs)) as Date))
                            .fontWeight(.semibold)
                        
                    }
                    Button("Enter") {
                        Task {
                          try await viewModel.addNewEntry(contestId: contest.id)
                        }
                
                    }
                    Button("Add Points") {
                        Task {
                            try await viewModel.addEntryPoints(contestId: contest.id)
                        }
                    }
                }.font(.footnote)
            }
        }
    }
}

