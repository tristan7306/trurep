//
//  AddEntryResponse.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/9/24.
//

import Foundation


struct AddEntryResponse: Decodable {
    let success: Int?
    let url: String?
    let error: Int?
    let message: String?
}
