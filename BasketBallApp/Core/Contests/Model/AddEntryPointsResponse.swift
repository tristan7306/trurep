//
//  AddEntryPointsResponse.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/9/24.
//

import Foundation


struct AddEntryPointsResponse: Decodable {
    let status: String
    let data: String?
    let message: String?
}
