//
//  AddEntry.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/9/24.
//

import Foundation

struct AddEntry: Codable {
    var email: String
}
