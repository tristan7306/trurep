//
//  AddEntryPoints.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/9/24.
//

import Foundation


struct AddEntryPoints: Codable {
    var email: String = "tristan7306@gmail.com"
    var points: Int = 1
    var description: String = "Shots taken"
}
