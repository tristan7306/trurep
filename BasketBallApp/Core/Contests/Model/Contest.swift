//
//  Contest.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/8/24.
//

import Foundation


struct Contests: Codable {
    let promotions: [Contest]
}

struct Contest: Codable, Identifiable {
    let id: String
    let title: String
    let type: String
    let startTs: Int
    let endTs: Int
    let timezone: String
    
    enum CodingKeys: String, CodingKey {
        case id, title, type, timezone
        case startTs = "start_ts"
        case endTs = "end_ts"
    }
}
