//
//  ContestDataService.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/8/24.
//

import Foundation

class ContestDataService {
    private let session: URLSession
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = [
            "x-api-key": "eaf0cc2c1c52d83e4d705bb58ae6ed74c07e347ca469edc7_42090u"
        ]
        self.session = URLSession(configuration: configuration)
    }
    

    
    func getContests() async throws -> [Contest] {
        let urlString = "https://app.viralsweep.com/api/promotions/48483"
        guard let url = URL(string: urlString) else { return [] }
        
        do {
            let (data, _) = try await session.data(from: url)
            let contests = try JSONDecoder().decode(Contests.self, from: data)
            return contests.promotions
        } catch {
            print("DEBUG: Error \(error.localizedDescription)")
            return []
        }
    }
    
    func postNewEntry(contestId: String, contestant: AddEntry) async throws {
        guard let encoded = try? JSONEncoder().encode(contestant) else {
            print("Failed to encode contestant")
            return
        }
        let urlString = "https://app.viralsweep.com/api/entries/\(contestId)"
        guard let url = URL(string: urlString) else { return }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        do {
            let (data, _) = try await session.upload(for: request, from: encoded)
            
            let decodeRes = try JSONDecoder().decode(AddEntryResponse.self, from: data)
     
            if decodeRes.error != nil {
                print("Error \(decodeRes.message ?? "")")
            }
          
        } catch {
            print("DEBUG: Post New Entry Error \(error.localizedDescription)")
        }
        
    }
    
    func addEntryPoints(contestId: String, entry: AddEntryPoints) async throws {
        guard let encoded = try? JSONEncoder().encode(entry) else {
            print("Failed to encode contestant")
            return
        }
        let urlString = "https://app.viralsweep.com/api/points/\(contestId)"
        guard let url = URL(string: urlString) else { return }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        do {
            let (data, _) = try await session.upload(for: request, from: encoded)
            
            let decodeRes = try JSONDecoder().decode(AddEntryPointsResponse.self, from: data)
     
            if decodeRes.status == "error" {
                print("Error \(decodeRes.message ?? "")")
            }
          
        } catch {
            print("DEBUG: Post Points Error \(error.localizedDescription)")
        }
        
    }
}
