//
//  ContestsViewModel.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/8/24.
//

import Foundation

@MainActor
class ContestsViewModel: ObservableObject {
    @Published var contests = [Contest]()
    @Published var contestant = AddEntry(email: "tristan7306@gmail.com")
    @Published var addEntry = AddEntryPoints()
    
    private let service = ContestDataService()
    
    init() {
        Task {try await fetchContests() }
    }
    
    
    func fetchContests() async throws {
        self.contests = try await service.getContests()
    }
    
    func addNewEntry(contestId: String) async throws {
        try await service.postNewEntry(contestId: contestId, contestant: self.contestant)
    }
    
    func addEntryPoints(contestId: String) async throws {
        try await service.addEntryPoints(contestId: contestId, entry: addEntry)
    }
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter
    }()
}
