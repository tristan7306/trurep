//
//  ContentView.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/6/24.
//

import SwiftUI


struct ContentView: View {
    var body: some View {
        TabView {
            ReceivedView()
                .badge(2)
                .tabItem {
                    Label("Tab1", systemImage: "tray.and.arrow.down.fill")
                }
            SentView()
                .tabItem {
                    Label("Tab2", systemImage: "tray.and.arrow.up.fill")
                }
            AccountView()
                .badge("!")
                .tabItem {
                    Label("Tab3", systemImage: "person.crop.circle.fill")
                }
            ContestView()
                .badge("1")
                .tabItem {
                    Label("Contest", systemImage: "basketball.fill")
                }
            
        }
    }
    
    
}


//#Preview {
//    ContentView()
//}
