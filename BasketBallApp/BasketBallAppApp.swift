//
//  BasketBallAppApp.swift
//  BasketBallApp
//
//  Created by Tristan Wyatt on 2/6/24.
//

import SwiftUI

@main
struct BasketBallAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
